document.addEventListener('DOMContentLoaded', function () {
    const searchInput = document.getElementById('searchInput');
    const content = document.getElementById('content');

    searchInput.addEventListener('input', function () {
        const searchTerm = searchInput.value.toLowerCase();

        // Clear previous content
        content.innerHTML = '';

        // Load and search each page
        searchPage('index.html', searchTerm);
        searchPage('page1.html', searchTerm);
        searchPage('page2.html', searchTerm);
    });

    async function searchPage(pageUrl, searchTerm) {
        try {
            const response = await fetch(pageUrl);
            const pageContent = await response.text();
            const pageDiv = document.createElement('div');
            pageDiv.innerHTML = pageContent;

            const paragraphs = pageDiv.getElementsByTagName('p');
            for (const paragraph of paragraphs) {
                const text = paragraph.textContent.toLowerCase();

                if (text.includes(searchTerm)) {
                    content.appendChild(paragraph.parentNode.cloneNode(true));
                }
            }
        } catch (error) {
            console.error('Error fetching page:', error);
        }
    }
});
