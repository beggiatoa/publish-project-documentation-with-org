(require 'package)

;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(setq package-user-dir (expand-file-name "./.packages"))

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;;; require built-ins
(require 'package-vc)
(require 'org)
(require 'ob-R)
(require 'ox)
(require 'ox-publish)

(setq user-full-name "Marco Prevedello")
(setq user-mail-address "marco.prevedello@outlook.it")

;;; Install and require extras
(unless (package-installed-p 's)
  (package-install 's))

(unless (package-installed-p 'htmlize)
  (package-install 'htmlize))
(require 'htmlize)

(add-to-list 'load-path "./.packages/org-tufte")
(require 'org-tufte)
(setq org-tufte-htmlize-code t)

;;; Org babel setting

(add-to-list 'org-src-lang-modes
             '("R" . R))
(add-to-list 'org-babel-load-languages
             '(R . t))

(setq org-confirm-babel-evaluate nil)

;;; Org publish settings

(setq org-publish-use-timestamps-flag t
      org-publish-timestamp-directory "./.org-cache/"
      org-export-with-section-numbers nil
      org-export-use-babel t
      org-export-with-toc nil
      org-export-with-smart-quotes t
      org-export-with-sub-superscripts '{}
      org-export-with-tags 'not-in-toc
      org-html-html5-fancy t
      org-html-self-link-headlines t
      org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-validation-link nil
      org-html-doctype "html5"
      ;; org-html-head-include-default-style nil
      )

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

;;;; Define publish project

(setq org-publish-project-alist
      `(("org-files"
         :base-directory "."
         :recursive t
         :base-extension "org"
         :exclude ,(regexp-opt '("README" "draft"))
         :publishing-directory "build/"
         :publishing-function org-html-publish-to-html
         :html-heat "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/stylesheet.css\" />"
         :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>")
        ("static-files"
         :base-directory "."
         :recursive t
         :base-extension ,site-attachments
         :exclude "build/"
         :publishing-directory "build/"
         :publishing-function org-publish-attachment)
        ("site" :components ("org-files" "static-files"))))

(defun my-publish-project ()
  (org-publish-project "site" t))

(provide 'publish)
;;; publish.el ends here
